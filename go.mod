module gitlab.com/diamondburned/6cord

go 1.12

require (
	github.com/alecthomas/chroma v0.6.3
	github.com/alecthomas/kong v0.1.16 // indirect
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1 // indirect
	github.com/andreyvit/diff v0.0.0-20170406064948-c7f18ee00883
	github.com/atotto/clipboard v0.1.2
	github.com/bwmarrin/discordgo v0.19.0 // indirect
	github.com/danieljoos/wincred v1.0.2 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/diamondburned/discordgo v1.1.2
	github.com/diamondburned/markdown v0.0.0-20190331092810-a5d3b972382c
	github.com/diamondburned/tcell v1.1.7-0.20190608162241-468b8880ec7a
	github.com/diamondburned/tview v1.2.2-0.20190611043218-c864b02cba8f
	github.com/disintegration/imaging v1.6.0
	github.com/gen2brain/beeep v0.0.0-20190603194150-07ff5e574111
	github.com/gobwas/ws v1.0.1 // indirect
	github.com/godbus/dbus v4.1.0+incompatible // indirect
	github.com/gopherjs/gopherjs v0.0.0-20190430165422-3e4dfb77656c // indirect
	github.com/gopherjs/gopherwasm v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.1 // indirect
	github.com/hashicorp/go-retryablehttp v0.5.4 // indirect
	github.com/jonas747/gojay v0.0.0-20181010205435-9081ac11e06c
	github.com/kr/pretty v0.1.0 // indirect
	github.com/kr/pty v1.1.4 // indirect
	github.com/kylelemons/godebug v1.1.0 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-sixel v0.0.0-20190320171103-a8fac8fa7d81
	github.com/mattn/go-tty v0.0.0-20190424173100-523744f04859
	github.com/nu7hatch/gouuid v0.0.0-20131221200532-179d4d0c4d8d // indirect
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/rumblefrog/discordgo v1.0.3 // indirect
	github.com/sahilm/fuzzy v0.1.0
	github.com/soniakeys/quant v1.0.0 // indirect
	github.com/stevenroose/gonfig v0.1.4
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/tadvi/systray v0.0.0-20190226123456-11a2b8fa57af // indirect
	github.com/valyala/fasttemplate v1.0.1
	github.com/zalando/go-keyring v0.0.0-20190603084339-02404fc6afd1
	gitlab.com/diamondburned/go-w3m v0.0.0-20190608163716-1b390b8a3d1f
	gitlab.com/diamondburned/ueberzug-go v0.0.0-20190521043425-7c15a5f63b06
	golang.org/x/crypto v0.0.0-20190605123033-f99c8df09eb5 // indirect
	golang.org/x/image v0.0.0-20190523035834-f03afa92d3ff // indirect
	golang.org/x/net v0.0.0-20190607181551-461777fb6f67 // indirect
	golang.org/x/sys v0.0.0-20190610200419-93c9922d18ae // indirect
	golang.org/x/tools v0.0.0-20190610231749-f8d1dee965f7 // indirect
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/toast.v1 v1.0.0-20180812000517-0a84660828b2 // indirect
)
